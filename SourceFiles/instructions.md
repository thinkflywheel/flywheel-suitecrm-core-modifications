# Instructions:

Store the Source (unedited) files for a given version in the respective version folder.

We're testing to see if the file changed in core between releases. If it changed, then we probably need to use the new file and to reinsert our customizations.
If it did not change, then we can continue to use our customized file (in the "PackageFiles" directory)

When a new Version comes out, do the following:

(In this example, we're going from 7.11.1 to 7.11.2)

1. Duplicate the previous version folder and rename with the new version. `Copy 7.11.1 to 7.11.2`
2. Download the new Version `Download 7.11.2` to your computer
3. Drop the new files into the folder 
4. Use Git to compare what has changed. 
    - If there are no changes for a given file, then you know that you don't need to do anything
    - Otherwise, you'll need to (probably) do customizations. (see below)
5. Commit the customized files into their place in the `PackageFiles` directory. Commit the source files in their respective versioned folder.
6. If you're adding a brand new file, be sure to add it to the Master Manifest

