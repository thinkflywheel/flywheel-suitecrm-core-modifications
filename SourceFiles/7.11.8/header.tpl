{include file="themes/SuiteP/tpls/_head.tpl"}
<body onMouseOut="closeMenus();">

{if $AUTHENTICATED}
    <div id="ajaxHeader">
        {include file="themes/SuiteP/tpls/_headerModuleList.tpl"}
    </div>
{/if}
{literal}
    <iframe id='ajaxUI-history-iframe' src='index.php?entryPoint=getImage&imageName=blank.png' title='empty'
            style='display:none'></iframe>
<input id='ajaxUI-history-field' type='hidden'>
<script type='text/javascript'>
    if (SUGAR.ajaxUI && !SUGAR.ajaxUI.hist_loaded) {
        YAHOO.util.History.register('ajaxUILoc', "", SUGAR.ajaxUI.go);
        {/literal}{if $smarty.request.module != "ModuleBuilder"}{* Module builder will init YUI history on its own *}
        YAHOO.util.History.initialize("ajaxUI-history-field", "ajaxUI-history-iframe");
        {/if}{literal}
    }
</script>
{/literal}
<!-- Start of page content -->
{if $AUTHENTICATED}
<div id="bootstrap-container"
     class="{if $THEME_CONFIG.display_sidebar && $smarty.cookies.sidebartoggle|default:'' != 'collapsed'}col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2{/if} main bootstrap-container">
    <div id="content" class="content">
        <div id="pagecontent" class=".pagecontent">
{/if}
