<?php

$manifest = [
    'readme' => '',
    'key' => 'PACKAGEKEY',
    'author' => 'Flywheel - TimS',
    'description' => 'PACKAGEDESCRIPTION',
    'icon' => '',
    'is_uninstallable' => false,
    'name' => 'PACKAGENAME',
    'published_date' => 'PUBLISHEDDATE',
    'type' => 'module',
    'version' => 'PACKAGEVERSION',
    'remove_tables' => '',
];

$installdefs = [
    'id' => 'PACKAGEID',
    'copy' => [
        [
            'from' => '<basepath>/include/SugarObjects/LanguageManager.php',
            'to' => 'include/SugarObjects/LanguageManager.php',
        ],
        [
            'from' => '<basepath>/ModuleInstall/ModuleInstaller.php',
            'to' => 'ModuleInstall/ModuleInstaller.php',
        ],
        [
            'from' => '<basepath>/lib/API/v8/Library/ModulesLib.php',
            'to' => 'lib/API/v8/Library/ModulesLib.php',
        ],
        [
            'from' => '<basepath>/themes/SuiteP/tpls/header.tpl',
            'to' => 'themes/SuiteP/tpls/header.tpl',
        ],
        [
            'from' => '<basepath>/lib/Search/ElasticSearch/ElasticSearchEngine.php',
            'to' => 'lib/Search/ElasticSearch/ElasticSearchEngine.php',
        ],
        [
            'from' => '<basepath>/lib/Search/ElasticSearch/ElasticSearchIndexer.php',
            'to' => 'lib/Search/ElasticSearch/ElasticSearchIndexer.php',
        ],
        [
            'from' => '<basepath>/Api/V8/BeanDecorator/BeanListRequest.php',
            'to' => 'Api/V8/BeanDecorator/BeanListRequest.php',
        ],
        [
            'from' => '<basepath>/Api/V8/Controller/BaseController.php',
            'to' => 'Api/V8/Controller/BaseController.php',
        ],
        [
            'from' => '<basepath>/Api/V8/Service/ModuleService.php',
            'to' => 'Api/V8/Service/ModuleService.php',
        ],
    ],
    'readme' => 'README.txt',

];