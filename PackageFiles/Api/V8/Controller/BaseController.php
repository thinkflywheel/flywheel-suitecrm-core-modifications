<?php

/**
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

/**
 * This file was modified by Flywheel on 2020-12-18 
 * 
 * Flywheel's modifications were made to address Bug #8979
 */

namespace Api\V8\Controller;

use Api\V8\JsonApi\Response\ErrorResponse;
use Slim\Http\Response as HttpResponse;

abstract class BaseController
{
    const MEDIA_TYPE = 'application/vnd.api+json';

    /**
     * @param HttpResponse $httpResponse
     * @param mixed $response
     * @param integer $status
     *
     * @return HttpResponse
     */
    public function generateResponse(
        HttpResponse $httpResponse,
        $response,
        $status
    ) {
        return $httpResponse
            ->withStatus($status)
            ->withHeader('Accept', static::MEDIA_TYPE)
            ->withHeader('Content-type', static::MEDIA_TYPE)
            ->withHeader('Cache-Control', 'no-cache')
            ->withHeader('pragma', 'no-cache')
            ->withHeader('Cache-Control', 'max-age=0')
            ->withHeader('Expires', 'Wed, 21 Oct 2015 07:28:00 GMT')
            ->write(
                json_encode(
                    $response,
                    JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
                )
            );
    }

    /**
     * @param HttpResponse $httpResponse
     * @param \Exception $exception
     * @param integer $status
     *
     * @return HttpResponse
     */
    public function generateErrorResponse(HttpResponse $httpResponse, \Exception $exception, $status)
    {
        $response = new ErrorResponse();
        $response->setStatus($status);
        $response->setDetail($exception->getMessage());
        $response->setException($exception);

        return $this->generateResponse($httpResponse, $response, $status);
    }
}
