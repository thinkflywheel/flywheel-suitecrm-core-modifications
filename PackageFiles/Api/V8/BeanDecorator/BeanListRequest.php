<?php

/**
* SugarCRM Community Edition is a customer relationship management program developed by
* SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
*
* SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
* Copyright (C) 2011 - 2018 SalesAgility Ltd.
*
* This program is free software; you can redistribute it and/or modify it under
* the terms of the GNU Affero General Public License version 3 as published by the
* Free Software Foundation with the addition of the following permission added
* to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
* IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
* OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
* details.
*
* You should have received a copy of the GNU Affero General Public License along with
* this program; if not, see http://www.gnu.org/licenses or write to the Free
* Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA.
*
* You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
* SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
*
* The interactive user interfaces in modified source and object code versions
* of this program must display Appropriate Legal Notices, as required under
* Section 5 of the GNU Affero General Public License version 3.
*
* In accordance with Section 7(b) of the GNU Affero General Public License version 3,
* these Appropriate Legal Notices must retain the display of the "Powered by
* SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
* reasonably feasible for technical reasons, the Appropriate Legal Notices must
* display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
*/

/**
 * This file was modified by Flywheel on 2019-10-08 
 * 
 * Flywheel's modifications were made to address Bug #8001
 */

namespace Api\V8\BeanDecorator;

class BeanListRequest
{
    /**
     * @var \SugarBean
     */
    protected $bean;

    /**
     * @var string
     */
    protected $orderBy = '';

    /**
     * @var string
     */
    protected $where = '';

    /**
     * @var integer
     */
    protected $offset = BeanManager::DEFAULT_OFFSET;

    /**
     * @var integer
     */
    protected $limit = -1;

    /**
     * @var integer
     */
    protected $max = BeanManager::DEFAULT_ALL_RECORDS;

    /**
     * @var integer
     */
    protected $deleted = 0;

    /**
     * @var boolean
     */
    protected $singleSelect = false;

    /**
     * @var array
     */
    protected $fields = [];

    /**
     * @param \SugarBean $bean
     */
    public function __construct(\SugarBean $bean)
    {
        $this->bean = $bean;
    }

    /**
     * @param string $orderBy
     *
     * @return BeanListRequest
     */
    public function orderBy($orderBy)
    {
        $this->orderBy = $orderBy;

        return $this;
    }

    /**
     * @param string $where
     *
     * @return BeanListRequest
     */
    public function where($where)
    {
        $this->where = $where;

        return $this;
    }

    /**
     * @param integer $offset
     *
     * @return BeanListRequest
     */
    public function offset($offset)
    {
        $this->offset = $offset;

        return $this;
    }

    /**
     * @param integer $limit
     *
     * @return BeanListRequest
     */
    public function limit($limit)
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @param integer $max
     *
     * @return BeanListRequest
     */
    public function max($max)
    {
        $this->max = $max;

        return $this;
    }

    /**
     * @param integer $deleted
     *
     * @return BeanListRequest
     */
    public function deleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * @param boolean $singleSelect
     *
     * @return BeanListRequest
     */
    public function singleSelect($singleSelect)
    {
        $this->singleSelect = $singleSelect;

        return $this;
    }

    /**
     * @param array $fields
     *
     * @return BeanListRequest
     */
    public function fields(array $fields)
    {
        $this->fields = $fields;

        return $this;
    }

    /**
     * @return BeanListResponse
     */
    public function fetch()
    {
        return new BeanListResponse($this->bean->get_list(
            $this->orderBy,
            $this->where,
            $this->offset,
            $this->limit,
            $this->max,
            $this->deleted,
            $this->singleSelect,
            $this->fields
        ));
    }
}
