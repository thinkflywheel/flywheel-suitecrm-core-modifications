# README
This package overwrites several core files in SuiteCRM.

LanguageManager:
This file controls how the language files are compiled. 
We need to change the order that the files are compiled. 
We made one change - we changed the order of the files in `$lang_paths` within the `refreshLanguage` function. 

ModuleInstaller:
This file, among other things, compiles the Extension files into the `*.ext.php` file. 
We want to order these files before they're compiled. Currently Suite doesn't do any ordering of them. It's completely OS dependent. 

We rewrote the `merge_files` function in order to support our sorting algorithms. 


Upgrades: 7.10.7 to 7.10.10
No major changes to the LanguageManger and ModuleInstaller file contents. Suite must have used a new Formatting library because all the files received spacing and format changes.
I reviewed these two files and didn't see anything substantial. 
I copy/pasted the core 7.10.10 files and then re-applied our changes. - TS 11/13/18

The `index.php` file was removed in 10.10 as they are deprecating that API. We're keeping it alive for a bit as we transition.
This package restores the file.

Upgrades 7.10.10. to 7.10.11

Attibutes.php was modified in order for the API to allow Updates. It can be removed when bug #6452 is resolved
routes.php was modified in order for the API to allow relationship unlinks. It can be removed when bug #6761 is resolved

SugarFieldFile.php was modified to fix a bug. This is resolved in 7.10.12 and above so it can be removed from future versions of this package. 

Attributes.php has been modified further - we want to allow blank values to be submitted. There is no bug open yet. 


### 7.10.11 to 7.11.8

- Removed `Attributes.php` as the bug has been fixed 
- Removed `routes.php` as the bug has been fixed
- Removed `SugarFieldFile.php` as the bug has been fixed
- Updated `LanguageManager.php` to bring in core changes (spacing) and keep Flywheel behavior
- Updated `ModuleInstaller.php` to bring in core changes (spacing and formatting) and keep Flywheel behavior
- Added `ModulesLib.php` - modified the `generatePaginationUrl` function. See notes.
- Added `header.tpl` - modified the hardcoded path to the `_head.tpl` file. See notes.
- Added `ElasticSearchEngine.php` and `ElasticSearchIndexer.php` - modified to use unique indices rather than the default of 'main'. Can be removed if #6800 is resolved.
      - Switched the visibility of the `createSearchParams` function from private to protected
- Added `SearchQuery.php` looks like we're setting a default search size
- Added `BeanListRequest.php` - this file sets the 'singleSelect' parameter on Sugarbean::get_list(). On Person type modules there are weird relate fields that cause the query to freak out. Keeping this setting off means that we can't see relate fields in the getRecords() api, but it prevents this freakout. Corresponding bug is #8001


### 7.11.8 to 7.11.18:

`LanguageManager.php` - minor formatting changes. We re-applied our customizations to the core file
`ModuleInstaller.php` - several minor formatting changes. We re-applied our customizations to the core file
`index.php` - no changes
`ModulesLib.php` - minor formatting. We copy-pasted these into our customized file
`header.tpl` - Core removed some fluff. We re-applied our customizations to the core file
`ElasticSearchEngine.php` - There were no changes in core, so we left our customized file alone
`ElasticSearchIndexer.php` - Many of our customizations were merged into core. However not all - so we applied our remaining customizations to the core file
`ElasitSearchModuleDataPuller.php` - Merged into core. Removed from this package
`SearchQuery.php` - Customizations merged into core. Removed from this package
`BeanListRequest.php` - There were no changes in core, so we left our customized file alone
`BaseController.php` - Due to #8979 we're ovwriting this file to add Cache-buster headers. Can be removed when bug is fixed
`ModuleService.php` - Due to #9024, we're overwriting this file. We added one line of code (ln 378). Can be removed when our PR is merged

### 7.11.18+
Notes : I think 8001 is fixed, so we may be able to remove `BeanListRequest`

 - Removed `index.php` was added back to core.


##### Notes:
`ModulesLib.php` : the core file miscommunicates with the Leage/Query library by trying to pass an array to the `withContent` function. It is designed to accept a string of query parameters. Since Suite uses such weird parameters, it's hard to generate this string. We changed to code to work properly by creating pairs and using the `createFromPairs` function. 

'header.tpl' - See [#7874](https://github.com/salesagility/SuiteCRM/issues/7874). Prior to 7.11.8, Suite could call a custom _head.tpl file. We use this to load things like sucrose. They changed some Smarty stuff and this broke. I'm modifying one line in this file. I could just move the whole file into custom, but then I'll forget to notice changes. 

### 7.11.18 to 7.12.3:
BaseController.php - There were no changes to core, so we left our customized file alone.
BeanListRequest.php - There were no changes to core, so we left our customized file alone.
ElasticSearchEngine.php - Major changes to core. We applied new customizations to add support for separate indexes for each unique instance of SuiteCRM.
ElasticSearchIndexer.php - Major changes to core. We applied new customizations to add support for separate indexes for each unique instance of SuiteCRM.
Header.tpl - There were changes to core. We re-applied our customizations to the core file.
LanguageManager.php - There were no changes to core, so we left our customized file alone.
ModuleInstaller.php - There were changes to core. We re-applied our customizations to the core file.
ModuleService.php - There were changes to core. We re-applied our customizations to the core file.
ModulesLib.php - There were no changes to core, so we left our customized file alone.

### 7.12.3 to 7.12.4:
LanguageManager.php - There were changes to core. We re-applied our customizations to the core file.

### 7.12.4 to 7.12.5:
No changes
