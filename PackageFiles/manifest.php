<?php

$manifest = [
    'readme' => '',
    'key' => 'Flywheel_SuiteCore_Modifications',
    'author' => 'Flywheel - TimS',
    'description' => 'This package installs a few modifications to Suite core. It is not upgrade safe and must be updated along with Suite. See Readme for specific changes.',
    'icon' => '',
    'is_uninstallable' => false,
    'name' => 'Flywheel_SuiteCore_Modifications',
    'published_date' => '2022-03-11T15:42:38',
    'type' => 'module',
    'version' => '7.12.5.1647034957993',
    'remove_tables' => '',
];

$installdefs = [
    'id' => 'Flywheel_SuiteCore_Modifications',
    'copy' => [
        [
            'from' => '<basepath>/include/SugarObjects/LanguageManager.php',
            'to' => 'include/SugarObjects/LanguageManager.php',
        ],
        [
            'from' => '<basepath>/ModuleInstall/ModuleInstaller.php',
            'to' => 'ModuleInstall/ModuleInstaller.php',
        ],
        [
            'from' => '<basepath>/lib/API/v8/Library/ModulesLib.php',
            'to' => 'lib/API/v8/Library/ModulesLib.php',
        ],
        [
            'from' => '<basepath>/themes/SuiteP/tpls/header.tpl',
            'to' => 'themes/SuiteP/tpls/header.tpl',
        ],
        [
            'from' => '<basepath>/lib/Search/ElasticSearch/ElasticSearchEngine.php',
            'to' => 'lib/Search/ElasticSearch/ElasticSearchEngine.php',
        ],
        [
            'from' => '<basepath>/lib/Search/ElasticSearch/ElasticSearchIndexer.php',
            'to' => 'lib/Search/ElasticSearch/ElasticSearchIndexer.php',
        ],
        [
            'from' => '<basepath>/Api/V8/BeanDecorator/BeanListRequest.php',
            'to' => 'Api/V8/BeanDecorator/BeanListRequest.php',
        ],
        [
            'from' => '<basepath>/Api/V8/Controller/BaseController.php',
            'to' => 'Api/V8/Controller/BaseController.php',
        ],
        [
            'from' => '<basepath>/Api/V8/Service/ModuleService.php',
            'to' => 'Api/V8/Service/ModuleService.php',
        ],
    ],
    'readme' => 'README.txt',

];