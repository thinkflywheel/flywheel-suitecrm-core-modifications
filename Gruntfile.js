module.exports = function(grunt) {
    let ts = (new Date()).getTime();
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        compress: {
            main: {
                options: {
                    archive: 'Zips/<%= pkg.name %><%= pkg.version %>.zip',
                    level: 6
                },
                files: [
                    // {src: ['PackageFiles/**']},
                    {
                        expand: true,
                        cwd: 'PackageFiles/',
                        src: ['**']
                    }, // makes all src relative to cwd
                ]
            }
        },
        watch: {
            files: ['PackageFiles/**','master_manifest.php'],
            tasks: ['bump','string-replace','compress', 'notify:zipped'],
        },
        notify: {
            watching: {
                options: {
                    enabled: true,
                    message: 'Watching Files!',
                    success: true,
                    duration: 1
                }
            },
            zipped: {
                options: {
                    message: 'Files Zipped!',
                    success: true,
                    duration: 1
                }
            }
        },
        bump: {
            options: {
                files: ['package.json'],
                updateConfigs: ['pkg'],
                commit: false,
                createTag: false,
                push: false,
            }
        },
        'string-replace': {
            dist: {
                files: {
                    'PackageFiles/manifest.php': 'master_manifest.php',
                },
                options: {
                    replacements: [
                    {
                        pattern: 'PACKAGEDESCRIPTION',
                        replacement: '<%= pkg.description %>'
                    },
                    {
                        pattern: 'PACKAGENAME',
                        replacement: '<%= pkg.name %>'
                    },
                    {
                        pattern: 'PACKAGEID',
                        replacement: '<%= pkg.name %>'
                    },
                    {
                        pattern: 'PACKAGEKEY',
                        replacement: '<%= pkg.name %>'
                    },
                    {
                        pattern: 'PACKAGEAUTHOR',
                        replacement: '<%= pkg.author %>'
                    },
                    {
                        pattern: 'PACKAGEVERSION',
                        replacement: '<%= pkg.version %>' + '.' + ts
                    },
                    {
                        pattern: 'PUBLISHEDDATE',
                        replacement: grunt.template.today('isoDateTime')
                    },

                    ]
                }
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-notify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-string-replace');
    grunt.loadNpmTasks('grunt-bump');
    //grunt.registerTask('default', ['compress']);

    grunt.registerTask('default', ['string-replace','compress', 'notify:zipped']);

};
