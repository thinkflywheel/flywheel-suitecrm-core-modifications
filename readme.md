# Overview:
This project holds SuiteCRM core files that Flywheel has modified. 

These files, along with SuiteCRM are licensed under the AGPLv3 license. 
SuiteCRM is maintained by SalesAgility and can be found here: https://github.com/salesagility/SuiteCRM

## Structure

**PackageFiles** : the ~10 files that are modified by Flywheel

**SourceFiles** : the original files directly from the SuiteCRM codebase. We use this with a diffchecker to see what (if anything) has changed

**Zips** : the compiled package

**Gruntfile.js** : Use this to compile the package

**master_manifest.php** : Use this to modify your package manifest. Grunt will fill in the variables (dates, versions, etc) and generate an actual manifest in the package.

## Package Notes.

Please put notes about file changes, etc in the `PackageFiles/Readme.txt` file. This is compiled and delivered with the package. 

I am assuming that we will need to generate this package for each Suite release.
Please update the Version in `package.json` whenever you generate a package. It should match the Suite version. 

You must download the overwritten files from Suite's releases and re-install our changes (if necessary). Do not let this code get behind the released versions. 

Please Tag each master commit with the corresponding version. 

Type `grunt` to build.
Save the package in VC.

Thanks. 